# Muffin Dev for NodeJS - Material Icons CLI

This package provide a CLI to operate over Google's Material Icons.

You can use this package without installing it using `npx`, or install it locally using `npm`:

```bash
npm i -g @muffin-dev/material-icons-cli
```

## Key Features

- Packing icons as atlases
- Extract single icon and variations (@todo)

## Usage

First, you'll need to download the Material Icons from their [GitHub repository](https://github.com/google/material-design-icons), and extract the files from the ZIP archive on your machine. Since there's more than 3,000 files, please understand that this process is not done automatically :)

Once done, run a terminal from the extracted directory, and run the commands you need from this CLI:

```bash
cd /path/to/material-design-icons-master
material-icons pack --folder="action" --style="rounded" --resolution=24dp --width=1024 --height=1024
```

This example command will pack all the icons from the folder `/png/action`, using the *rounded* aspect, with a resolution of 24 dp, and the extracted atlases are themselves splitted in images of 1024x1024 pixels.

See below for more details about the commands and options.

## Commands

All commands assumes that your terminal is placed in the directory that contains the [Material Icons' repository](https://github.com/google/material-design-icons) files (see *Usage* section for more informations).

### `get`

This command will extract and process icons individually.

#### Arguments

- All the common arguments (see *Common arguments* section below).

#### Options

- All the common options (see *Common options* section below).
- `--icon` (string): The icon you want to pack. You can also specify more than one icon by separating them by commas (`,`). If omitted, the command will fail unless the `--all, -a` option is enabled. Example: if you pass `warning`, only the icon at `/png/alert/warning` will be processed.

### `pack`

This command will pack an entire folder of icons (or even every one of them at once) onto atlases, in order to create bigger files with a lot of icons at once.

#### Arguments

- All the common arguments (see *Common arguments* section below).

#### Options

- All the common options (see *Common options* section below).
- `--compact, -c` (boolean): By default, the generated atlas images contain the icons of a single category, meaning that if there's more than one folder being packed but there's not enough icon to fill an atlas, the atlas is generated and another one is created for the next folder. If this option is enabled, the generated atlas images will contain the most icons possible, making them able to contain the icons of more than one folder at once.
- `--folder` (string): The folder you want to pack. You can also specify more than one folder by separating them by commas (`,`). If omitted, the command will fail unless the `--all, -a` option is enabled. Example: if you pass `device`, only the icons from `/png/device` will be packed.
- `--height` (number): The output atlas images height. If omitted, `2048` px is used by default.
- `--mapping` (string): The format of the mapping files generated with the atlases in order to find the icons by name. Available values: `json` (please request any other desired format). If omitted, `json` is used by default.
- `--offset` (number): The number of pixels that the icons are moved on the left and right axis on the generated atlases. This adds a margin on the top and left borders. If omitted, `0` is used.
- `--padding` (number): The margin around each icon on the generated atlases. If omitted, `0` is used.
- `--width` (number): The output atlas images width. If omitted, `2048` px is used by default.

### Common arguments

- `[source]` (path, optional): The directory that contains the Material Icons' folders. By default, the shell's active directory is used.

### Common options

- `--all, -a` (boolean): If enabled, all the icons or folders will be processed, unless they are mentionned explicitly in the command you run.
- `--color` (string): The color of the icons. Available values: `black` and `white`. If omitted, `black` is used by default.
- `--double, -d` (boolean): If enabled, doubles the size of the queried icons (using the `/2x` folders).
- `--name` (string): The name of the output atlas images. If omitted, `%Category_%Name_%Color_##` is used. Note that you can use symbols in that name for automatizations:
    - `%Category` (or `%category` for lowercase): The category of the icon, basically the name of its folder. Examples: `Action`, `Communication`, ...
    - `%Color` (or `%color` for lowercase): The color used to process the icons. Examples: `Black`, `White`, ...
    - `%Name` (or `%name` for lowercase): The name of the icon. Examples: `3DRotation`, `Accessibility`, ...
    - `%resolution`: The resolution used to process the icons. Examples: `18dp`, `24dp`, ...
    - `%Style` (or `%style` for lowercase): The style used to process the icons. Examples: `Fill`, `Outline`, ...
    - `###`: The increment value of the generated atlas. By default, if more than one atlas is required to contain all the icons, the increment value is added as a suffix. But every `#` characters in the provided name will be replaced by that increment value. Also, if several `#` characters are found groupped together, leading 0s are added in order to match that number of characters. Example, at iteration 12, `#` and `##` are replaced by `12`, and `####` is replaced by `0012`.
- `--resolution` (string): The resolution of the icons you want to pack. Available values: `18dp`, `24dp`, `36dp` and `48dp`. If omitted, the `24dp` resolution is used by default.
- `--style` (string): Defines the icon style you want to use. Available values: `fill`, `outline`, `round`, `sharp` and `twotone`. If omitted, the `fill` style is used by default.

## License

Muffin Dev Libraries © 2022 by MuffinDev is licensed under Attribution 4.0 International. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0

---

Copyright Muffin Dev © 2022