#!/usr/bin/env node
import { Command } from 'commander';

import GetCommand from './get';
import PackCommand from './pack';

const program = new Command('material-icons-cli');

GetCommand(program);
PackCommand(program);

program.parse(process.argv);