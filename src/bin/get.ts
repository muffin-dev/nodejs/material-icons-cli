import { Command } from 'commander';
import { IGetOptions, get, GetOptions } from '../lib';
import { DEFAULT_ICON_COLOR, DEFAULT_ICON_RESOLUTION, DEFAULT_ICON_STYLE } from '../lib/internal';

export default function(program: Command) {
  program
    .command('get')
    .description('This command will extract and process icons individually.')
    .argument('<source>', 'Path to the directory that contains the Material Icons\' folders.')
    .argument('[output]', 'Path to the directory that will contain the processed assets.')
    .option('-a --all', 'If enabled, all the icons or folders will be processed, unless they are mentionned explicitly in the command you run.', false)
    .option('-d --double', 'If enabled, doubles the size of the queried icons (using the `/2x` folders).')
    .option('--color <color>', 'The color of the icons', DEFAULT_ICON_COLOR)
    .option('--icon <folder>', 'Names of the icon(s) you want to process. Specify several ones by separating them with commas (,).')
    .option('--name <name>', 'The name of the output images.', '%Category_%Name_%Color')
    .option('--resolution <resolution>', 'The resolution of the icons you want to get.', DEFAULT_ICON_RESOLUTION)
    .option('--style <style>', 'Defines the icon style you want to use.', DEFAULT_ICON_STYLE)
    .action(async (source: string, output: string, args: IGetOptions) => {
      args = new GetOptions(args);
      
      try {
        await get(source, output, args);
      }
      catch (error) {
        console.error(error);
      }
    });
}