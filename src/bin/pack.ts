import { Command } from 'commander';
import { IPackOptions, pack, PackOptions } from '../lib';
import { DEFAULT_ICON_COLOR, DEFAULT_ICON_RESOLUTION, DEFAULT_ICON_STYLE } from '../lib/internal';

export default function(program: Command) {
  program
    .command('pack')
    .description('Packs entire folder(s) of icons')
    .addHelpText('after', 'This command will pack an entire folder of icons (or even every one of them at once) onto atlases, in order to create bigger files with a lot of icons at once.')
    .argument('<source>', 'Path to the directory that contains the Material Icons\' folders.')
    .argument('[output]', 'Path to the directory that will contain the processed assets.')
    .option('-a --all', 'If enabled, all the icons or folders will be processed, unless they are mentionned explicitly in the command you run.', false)
    .option('-c --compact', 'Allow generated atlases to contain icons of multiple categories (and so every atlas contain the most icons possible.', false)
    .option('-d --double', 'If enabled, doubles the size of the queried icons (using the `/2x` folders).')
    .option('--color <color>', 'The color of the icons', DEFAULT_ICON_COLOR)
    .option('--folder <folder>', 'Names of the icons\' folder(s) you want to pack. Specify several ones by separating them with commas (,).')
    .option('--height <height>', 'The output atlas images height.', '2048')
    .option('--mapping <mapping>', 'The format of the mapping files generated with the atlases in order to find the icons by name.', 'json')
    .option('--name <name>', 'The name of the output atlas images.', 'Atlas_%Category_%Color_##')
    .option('--offset <offset>', 'The number of pixels that the icons are moved on the left and right axis on the generated atlases.', '0')
    .option('--padding <padding>', 'The margin around each icon on the generated atlases.', '0')
    .option('--resolution <resolution>', 'The resolution of the icons you want to pack.', DEFAULT_ICON_RESOLUTION)
    .option('--style <style>', 'Defines the icon style you want to use.', DEFAULT_ICON_STYLE)
    .option('--width <width>', 'The output atlas images width.', '2048')
    .action(async (source: string, output: string, args: IPackOptions) => {
      args = new PackOptions(args);

      try {
        await pack(source, output, args);
      }
      catch (error) {
        console.error(error);
      }
    });
}