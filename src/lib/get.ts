import { overrideObject } from '@muffin-dev/js-helpers';
import { BaseOptions } from './internal';

/**
 * @class Represents the options for the get() function.
 */
 export class GetOptions extends BaseOptions {
  public icon: string | string[];

  constructor(args: IGetOptions) {
    super(args);
    overrideObject(this, args);
  }
}

/**
 * @interface IGetOptions Represents the options of the get() function.
 */
export interface IGetOptions extends Partial<GetOptions> { }

/**
 * Extracts and process icons individually.
 * @param source Path to the directory that contains the Material Icons' folders.
 * @param options Options for processing the icons.
 */
export async function get(source: string, output: string, options: IGetOptions): Promise<void> {
  console.warn('@todo get()');
}