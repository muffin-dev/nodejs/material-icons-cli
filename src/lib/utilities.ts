/**
 * Splits the given string by trimmed separator (, ; |).
 * @param str The string to split.
 * @returns Returns the processed string.
 */
export function split(str: string): string[] {
  return str.split(/\s*[,;|]\s*/);
}

/**
 * Set the first character of the given string uppercase.
 * @param str The string to process.
 * @returns Returns the processed string.
 */
export function upperFirstLetter(str: string) {
  return str.length > 0 ? str.charAt(0).toUpperCase() + str.slice(1) : '';
}

/**
 * "Pads" a number by adding leading 0.
 * @param value The number you want to pad.
 * @param length The expected length of the value.
 * @returns Returns the processed string.
 */
export function pad(value: number, length: number) {
  let str = value.toString();
  while (str.length < length) {
    str = '0' + value;
  }
  return str;
}