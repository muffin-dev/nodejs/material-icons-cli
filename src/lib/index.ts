export type TIconStyle = 'fill' | 'outline' | 'round' | 'sharp' | 'twotone';
export type TIconColor = 'black' | 'white';
export type TIconResolution = '18dp' | '24dp' | '36dp' | '48dp';
export type TIconsMappingFormat = 'json';

export * from './get';
export * from './pack';
export * from './utilities';
