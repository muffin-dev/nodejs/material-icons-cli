import { overrideObject } from '@muffin-dev/js-helpers';
import { readdirAsync } from '@muffin-dev/node-helpers';
import { existsSync } from 'fs';
import { join } from 'path';
import { TIconColor, TIconResolution, TIconStyle } from '.';
import { pad, split, upperFirstLetter } from './utilities';

export const DEFAULT_ICON_STYLE: TIconStyle = 'fill';
export const DEFAULT_ICON_RESOLUTION: TIconResolution = '24dp';
export const DEFAULT_ICON_COLOR: TIconColor = 'black';
export const DEFAULT_ICON_NAME = '%Category_%Name_%Color_##';

/**
 * @class Represents the common options for the available commands.
 */
export class BaseOptions {
  public all: boolean = false;
  public color: TIconColor = DEFAULT_ICON_COLOR;
  public double: boolean = false;
  public name: string = null;
  public resolution: TIconResolution = DEFAULT_ICON_RESOLUTION;
  public style: TIconStyle = DEFAULT_ICON_STYLE;

  constructor(args: IBaseOptions) {
    overrideObject(this, args);
  }
}

/**
 * @interface IBaseOptions Represents the common options of the command function.
 */
export interface IBaseOptions extends Partial<BaseOptions> { }

/**
 * @interface IIconInfo Informations about an icon file to process.
 */
export interface IIconInfo {
  category: string;
  name: string;
  path: string;
}

/**
 * @interface IIconStyleInfo Groupes informations about an icon style.
 */
interface IIconStyleInfo {
  type: TIconStyle;
  directory: string;
  prefix: string;
}

/**
 * @const ICON_STYLES Informations about the various icon styles.
 */
export const ICON_STYLES: Record<TIconStyle, IIconStyleInfo> = {
  fill:     { type: 'fill',     directory: 'materialicons',         prefix: 'baseline' },
  outline:  { type: 'outline',  directory: 'materialiconsoutlined', prefix: 'outline' },
  round:    { type: 'round',    directory: 'materialiconsround',    prefix: 'round' },
  sharp:    { type: 'sharp',    directory: 'materialiconssharp',    prefix: 'sharp' },
  twotone:  { type: 'twotone',  directory: 'materialiconstwotone',  prefix: 'twotone' },
};

/**
 * 
 * @param source The absolute path to the /png directory of the Material Icons' repository. Assumes this path is valid.
 * @param categories Filters the icon categories from which you want to get thee icons. If undefined, this function will browse icons in
 * all categories.
 * @param icons Filters the icons you want to get. If undefined, this function will get all icons.
 * @param options The options used to process the icon files
 */
export async function getIconFiles(source: string, categories: string | string[], icons: string | string[], options: Pick<IBaseOptions, "resolution" | "style" | "double">): Promise<IIconInfo[]> {
  // Fix categories list
  if (categories && !Array.isArray(categories)) {
    categories = split(categories);
  }
  // Fix icons list
  if (icons && !Array.isArray(icons)) {
    icons = split(icons);
  }
  // Fix style option
  if (!options.style) {
    options.style = DEFAULT_ICON_STYLE;
  }
  // Fix resolution option
  if (!options.resolution) {
    options.resolution = DEFAULT_ICON_RESOLUTION;
  }

  const outputIconInfos = new Array<IIconInfo>();

  /**
   * Material Icons' directory layout:
   * /png (source)
   *    /category
   *      /icon
   *        /style (materialicons, materialiconsoutlined, materialiconsround, materialiconssharp or materialiconstwotone)
   *          /resolution (18dp, 24dp, 36dp or 48dp)
   *            /size (1x or 2x)
   *              /[style prefix]_[icon directory name]_black_[resolution].png
   */

  const categoryDirs = await readdirAsync(source, true, false);
  // For each category
  for (const categoryDir of categoryDirs) {
    // Skip if the user wants to target specific categories, but the current one doesn't match
    if (categories && !categories.includes(categoryDir.name)) {
      continue;
    }

    const iconDirs = await readdirAsync(categoryDir.path, true, false);
    // For each icon in the current directory
    for (const iconDir of iconDirs) {
      // Skip if the user wants to target specific icons, but the current one doesn't match
      if (icons && !icons.includes(iconDir.name)) {
        continue;
      }

      const iconFileName = `${ICON_STYLES[options.style].prefix}_${iconDir.name}_black_${options.resolution}.png`;
      const path = join(iconDir.path, ICON_STYLES[options.style].directory, options.resolution, options.double ? '2x' : '1x', iconFileName);
      // Skip if the icon can't be found using the given options
      if (!existsSync(path)) {
        console.warn(`Icon ${iconDir.name} can't be found at "${path}" with the given options.`);
        continue;
      }

      outputIconInfos.push({
        name: iconDir.name,
        category: categoryDir.name,
        path: path
      });
    }
  }

  return outputIconInfos;
}

/**
 * Generates a name for an icon file.
 * You can use severay symbols:
 *    - %category (or %Category for first-letter uppercase) -> The icon's category name
 *    - %name (or %Name for first-letter uppercase) -> The icon's name
 *    - %color (or %Color for first-letter uppercase) -> The color name
 *    - %style (or %Style for first-letter uppercase) -> The style name
 *    - %resolution -> The resolution value
 *    - ###: The increment value of the generated image. Every # characters in the provided name will be replaced by that increment value.
 * Also, if several # characters are found groupped together, leading 0s are added in order to match that number of characters. Example, at
 * iteration 12, # and ## are replaced by 12, and #### is replaced by 0012.
 * @param icon The informations about the icon for which you want to generate a name.
 * @param options The options used to generate a name.
 * @param iteration The iteration number, if applicable.
 * @returns Returns the generated name.
 */
export function makeName(icon: IIconInfo, options: IBaseOptions, iteration: number = 0) {
  if (!options.name) {
    options.name = DEFAULT_ICON_NAME;
  }

  options.color = options.color || DEFAULT_ICON_COLOR;
  let str = options.name;

  // replace iteration string
  if (iteration || iteration === 0) {
    const match = options.name.match(/(#+)/);
    if (match) {
      str = str.replace(/#+/, pad(iteration, match[1].length));
    }
  }

  return str
    .replace('%Category', upperFirstLetter(icon.category))
    .replace('%category', icon.category)
    .replace('%Name', upperFirstLetter(icon.name))
    .replace('%name', icon.name)
    .replace('%Color', upperFirstLetter(options.color))
    .replace('%color', options.color)
    .replace('%Style', upperFirstLetter(options.style))
    .replace('%style', options.style)
    .replace('%resolution', options.resolution)
}