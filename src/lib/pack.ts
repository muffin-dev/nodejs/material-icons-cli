import { join } from 'path';
import { existsSync, unlinkSync } from 'fs';
import sharp from 'sharp';
import { overrideObject } from '@muffin-dev/js-helpers';
import { asAbsolute, readdirAsync, writeFileAsync } from '@muffin-dev/node-helpers';
import { BaseOptions, getIconFiles, IIconInfo, makeName } from './internal';
import { TIconsMappingFormat } from '.';

sharp.cache(false);

const ATLAS_COUNT_MIN_INDEX = 1;

/**
 * @class Represents the options for the pack() function.
 */
 export class PackOptions extends BaseOptions {
  public compact: boolean = false;

  public folder: string | string[] = null;
  public height: number = 2048;
  public mapping: TIconsMappingFormat = 'json';
  public offset: number = 0;
  public padding: number = 0;
  public width: number = 2048;

  constructor(args: IPackOptions) {
    super(args);
    overrideObject(this, args);
  }
}

/**
 * @interface IPackOptions Represents the options of the pack() function.
 */
export interface IPackOptions extends Partial<PackOptions> { }

/**
 * @interface IAtlasMapping Represents the mapping of a single atlas.
 */
export interface IAtlasMapping {
  atlas: string;
  iconSize: number;
  icons: IIconMapping[];
}

/**
 * @interface IIconMapping Represents a signle icon mapped on an atlas.
 */
export interface IIconMapping {
  name: string;
  category: string;
  x: number;
  y: number;
}

/**
 * Packs folder(s) of icons onto atlas images.
 * @param source Path to the directory that contains the Material Icons' folders.
 * @param options Options for processing the icons.
 */
export async function pack(source: string, output: string, options: IPackOptions = null): Promise<void> {
  source = !source ? process.cwd() : asAbsolute(source);
  output = !output ? process.cwd() : asAbsolute(output);
  if (!options) { options = new PackOptions({ }); }

  const pngDir = join(source, 'png');
  if (!existsSync(pngDir)) {
    throw new Error(`No /png directory found in the source directory (${pngDir})`);
  }

  // Cancel if the user doesn't want to query all categories but no category has been mentionned
  if (!options.folder && !options.all) {
      throw new Error('No folder mentionned for querying the icons. Please enable the "all" option if you want to query all the available categories.');
  }
  
  const icons = await getIconFiles(pngDir, (options.folder as string[]), null, options);
  if (icons.length <= 0) {
    throw new Error('No icons to process.');
  }

  const mappings = await generateAtlases(output, icons, options);
  const mappingFilePath = join(output, 'mappings.json');
  await writeFileAsync(mappingFilePath, JSON.stringify({ mappings }));
  console.log('Mapping file created at ' + mappingFilePath);
}

/**
 * Generate the atlas images for the given icons.
 * @param output The absolute path to the output directory.
 * @param icons The icons to process.
 * @param options The options to use for processing the icons.
 */
async function generateAtlases(output: string, icons: IIconInfo[], options: IPackOptions): Promise<IAtlasMapping[]> {
  if (icons.length <= 0) {
    return;
  }

  // Get the size of the icons to process
  const metadata = await sharp(icons[0].path).metadata();
  const size = Math.max(1, metadata.width, metadata.height);

  // Fix size and position options
  options.width = Math.max(size, Number(options.width) || 2048);
  options.height = Math.max(size, Number(options.height) || 2048);
  options.offset = Number(options.offset) || 0;
  options.padding = Number(options.padding) || 0;

  // Compute the number of icons per atlases
  const nbIconsPerRow = Math.max(1, Math.floor((options.width - options.offset) / (size + options.padding)));
  const nbIconsPerColumn = Math.max(1, Math.floor((options.height - options.offset) / (size + options.padding)));
  const nbIconsPerAtlas = nbIconsPerRow * nbIconsPerColumn;

  // Setup atlas processing
  const mappings = new Array<IAtlasMapping>();
  let atlasCount = ATLAS_COUNT_MIN_INDEX;
  let currentAtlasFirstIconIndex = 0;
  let i = 0;

  // For each icon to process
  for (; i < icons.length; i++) {
    if
    (
      // If the range from the last index to the current one is higher than the maximum number of icons on a single atlas
      i - currentAtlasFirstIconIndex + 1 >= nbIconsPerAtlas ||
      // Or if the category of the first icon to include in the current atlas has a different category than the current one, but compact
      // mode is disabled
      (!options.compact && icons[currentAtlasFirstIconIndex].category != icons[i].category)
    ) {
      const mapping = await generateSingleAtlas(output, size, atlasCount++, icons.slice(currentAtlasFirstIconIndex, i + 1), options);
      if(mapping) {
        mappings.push(mapping);
      }
      currentAtlasFirstIconIndex = i + 1;

      // Reset the atlas iteration value if the next one will contain another category
      if (options.compact && icons[currentAtlasFirstIconIndex].category != icons[i].category) {
        atlasCount = ATLAS_COUNT_MIN_INDEX;
      }
    }
  }

  // If there's remaining icons that have not been included into atlases
  if (currentAtlasFirstIconIndex < i) {
    const mapping = await generateSingleAtlas(output, size, atlasCount++, icons.slice(currentAtlasFirstIconIndex, i + 1), options);
    if(mapping) {
      mappings.push(mapping);
    }
  }

  return mappings;
}

/**
 * Generate a single atlas image with the given icons.
 * @param output The absolute path to the output directory.
 * @param iconSize The size of an icon.
 * @param iteration The current index of the atlas iteration.
 * @param icons The range of the icons to include in this atlas.
 * @param options The options to use for processing the icons.
 * @returns Returns the generated atlas' mappings.
 */
async function generateSingleAtlas(output: string, iconSize: number, iteration: number, icons: IIconInfo[], options: Pick<IPackOptions, 'width' | 'height' | 'name' | 'offset' | 'padding' | 'color'>): Promise<IAtlasMapping> {
  const nbIconsPerRow = Math.max(1, Math.floor((options.width - options.offset) / (iconSize + options.padding)));
  const nbRows = Math.ceil(icons.length / nbIconsPerRow);

  const atlasName = makeName(icons[0], options, iteration);
  const atlasOutputPath = join(output, `${atlasName}.png`);
  const tmpAtlasOutputPath = join(output, `tmp_${atlasName}.png`);

  const mappings: IAtlasMapping = {
    atlas: atlasOutputPath,
    iconSize: iconSize,
    icons: new Array<IIconMapping>()
  };

  let atlas = sharp({
    create: {
      width: options.width,
      height: options.height,
      channels: 4,
      background: { r: 0, g: 0, b: 0, alpha: 0 }
    }
  });

  let canContinue = true;
  for (let y = 0; y < nbRows; y++) {
    if (!canContinue) {
      break;
    }

    for (let x = 0; x < nbIconsPerRow; x++) {
      const index = y * nbIconsPerRow + x;
      if (index >= icons.length) {
        canContinue = false;
        break;
      }

      const mapping: IIconMapping = {
        x: options.offset + x * (iconSize + options.padding),
        y: options.offset + y * (iconSize + options.padding),
        name: icons[index].name,
        category: icons[index].category,
      }

      mappings.icons.push(mapping);
    }
  }

  // Create tmp atlaas
  await atlas
    .composite(icons.map((icon, index) => {
      return { input: icon.path, left: mappings.icons[index].x, top: mappings.icons[index].y }
    }))
    .toFile(tmpAtlasOutputPath);

  atlas = sharp(tmpAtlasOutputPath);
  if (options.color === 'white') {
    atlas.negate({ alpha: false });
  }

  // Create final atlas image
  await atlas.toFile(atlasOutputPath);
  // Delete tmp atlas
  unlinkSync(tmpAtlasOutputPath);

  console.log('Atlas image generated at ' + atlasOutputPath);
  return mappings;
}